Bonus
=====

-   [Advanced R](http://adv-r.had.co.nz) by Hadley Wickham

Week 11
=======

11/9/2018

-   Amplicon Bioinformatics
    -   [DADA2 Lecture](lectures/dada2_pipeline.pdf)
    -   [DADA2 Tutorial](lessons/dada2_tutorial_1_6.md): please work through each of the steps in this tutorial

Week 10
=======

11/2/2018

1.  Catch up!
    -   [Challenge 4](https://github.com/IBIEM/challenge_4) due today
    -   If you are all done: [DADA2 Tutorial](lessons/dada2_tutorial_1_6.md)
2.  [DADA2 Lecture](lectures/dada2_pipeline.pdf)

Week 9
======

10/26/2018

1.  Catch up!
    -   [Challenge 3](https://github.com/IBIEM/challenge_3) due today
    -   [Challenge 4](https://github.com/IBIEM/challenge_4) due next week
    -   If you are all done: [DADA2 Tutorial](lessons/dada2_tutorial_1_6.md)

Week 8
======

10/19/2018

1.  Catch up!
    -   [Challenge 3](https://github.com/IBIEM/challenge_3) due next week
    -   [Challenge 4](https://github.com/IBIEM/challenge_4) due in two weeks
    -   If you are all done: [DADA2 Tutorial](lessons/dada2_tutorial_1_6.md)
2.  Additional Resources?

Week 7
======

10/12/2018

1.  *DUE:* [Challenge 2](https://github.com/IBIEM/challenge_2)
2.  [Paths in R, RStudio, and Unix](misc/paths.md)
3.  [Challenge 4](https://github.com/IBIEM/challenge_4)
4.  [The Unix Shell Bonus](http://swcarpentry.github.io/shell-extras/)
    1.  [Manual Pages](http://swcarpentry.github.io/shell-extras/01-man-pages/)
    2.  [Transferring Files](http://swcarpentry.github.io/shell-extras/03-file-transfer/)
    3.  [Permissions](http://swcarpentry.github.io/shell-extras/04-permissions/)
    4.  [Job control](http://swcarpentry.github.io/shell-extras/06-job-control/)
    5.  [Shell Variables](http://swcarpentry.github.io/shell-extras/08-environment-variables/)
    6.  *Optional* [Working Remotely](http://swcarpentry.github.io/shell-extras/02-ssh/)

Week 6
======

10/5/2018

1.  [Challenge 3](https://github.com/IBIEM/challenge_3)
2.  [The Unix Shell](http://swcarpentry.github.io/shell-novice/)
    1.  [Loops](http://swcarpentry.github.io/shell-novice/05-loop/index.html)
    2.  [Shell Scripts](http://swcarpentry.github.io/shell-novice/06-script/index.html)
    3.  [Finding Things](http://swcarpentry.github.io/shell-novice/07-find/index.html)
    4.  [The Unix Shell: Summary of Basic Commands](http://swcarpentry.github.io/shell-novice/reference/)

Week 5
======

9/28/2018

1.  [Challenge 2](https://github.com/IBIEM/challenge_2)
2.  [The Unix Shell](http://swcarpentry.github.io/shell-novice/) You can work on the Unix Shell lessons in an R Notebook (my preference) or in the RStudio Terminal. If you use the R Notebook, just use *Bash* chunks instead of *R* chunks. The one thing that you cannot do with *Bash* chunks in an R Notebook that you can do at a terminal is run programs that expect you to interact with them. Some examples of this are `nano` and `rm -i` (`rm` without `-i` works fine within a notebook). In place of `nano`, you can use the RStudio editor instead, just open a new "Text File" within RStudio.
    1.  Download Data for Lessons: Run the two lines below in a *bash* chunk to download the data necessary for the Unix Shell lessons

    ``` bash
    wget https://swcarpentry.github.io/shell-novice/data/data-shell.zip
    unzip data-shell.zip
    ```

    1.  [Introducing the Shell](http://swcarpentry.github.io/shell-novice/01-intro/index.html)
    2.  [Navigating Files and Directories](http://swcarpentry.github.io/shell-novice/02-filedir/index.html)
    3.  [Working With Files and Directories](http://swcarpentry.github.io/shell-novice/03-create/index.html)
    4.  [Pipes and Filters](http://swcarpentry.github.io/shell-novice/04-pipefilter/index.html)

Week 4
======

9/21/2018

1.  [R Lesson Assignments](http://swcarpentry.github.io/r-novice-gapminder/)
    1.  [Dataframe Manipulation with tidyr](http://swcarpentry.github.io/r-novice-gapminder/14-tidyr/)
    2.  [Producing Reports With knitr](http://swcarpentry.github.io/r-novice-gapminder/15-knitr-markdown/)
    3.  [Writing Good Software](http://swcarpentry.github.io/r-novice-gapminder/16-wrap-up/)

Week 3
======

9/14/2018

1.  Added [Reference Information](misc/reference_info.md) on [Downloading](misc/downloading.md) into IBIEM environment
2.  [Challenge 1](https://github.com/IBIEM/challenge_1) *DUE!*
3.  Finish with [Week 1 tasks](#week-1) and [Week 2 tasks](#week-2) if not done
4.  [R Lesson Assignments](http://swcarpentry.github.io/r-novice-gapminder/)
    1.  [Vectorization](http://swcarpentry.github.io/r-novice-gapminder/09-vectorization/)
    2.  [Functions Explained](http://swcarpentry.github.io/r-novice-gapminder/10-functions/)
    3.  [Writing Data](http://swcarpentry.github.io/r-novice-gapminder/11-writing-data/)
    4.  [Dataframe Manipulation with dplyr](http://swcarpentry.github.io/r-novice-gapminder/13-dplyr/)

Week 2
======

9/7/2018

1.  [Github Notes](misc/github_notes.md)
2.  Added [Reference Information](misc/reference_info.md) about [Github, Bitbucket, and Gitlab](misc/github_notes.md)
3.  Continue with [Week 1 tasks](#week-1) if not done
4.  [R Lesson Assignments](http://swcarpentry.github.io/r-novice-gapminder/)
    1.  [Subsetting Data](http://swcarpentry.github.io/r-novice-gapminder/06-data-subsetting/)
    2.  [Control Flow](http://swcarpentry.github.io/r-novice-gapminder/07-control-flow/)
    3.  [Creating Publication-Quality Graphics](http://swcarpentry.github.io/r-novice-gapminder/08-plot-ggplot2/)

Week 1
======

8/31/2018

1.  This week in reproducbile research
2.  Fear of loss
3.  Flipping
4.  Finish [Git Overview](bootcamp/040_git_overview.md)
    1.  [Collaborating](bootcamp/040_git_overview.md#collaborating)
    2.  [Conflicts](bootcamp/040_git_overview.md#conflicts)
    3.  [Team Conflicts](bootcamp/040_git_overview.md#team-conflicts)
5.  [Challenge 1](https://github.com/IBIEM/challenge_1)
6.  [R Lesson Assignments](http://swcarpentry.github.io/r-novice-gapminder/)
    1.  [Introduction to R and RStudio](http://swcarpentry.github.io/r-novice-gapminder/01-rstudio-intro/)
    2.  [Project Management With RStudio](http://swcarpentry.github.io/r-novice-gapminder/02-project-intro/)
    3.  [Seeking Help](http://swcarpentry.github.io/r-novice-gapminder/03-seeking-help/)
    4.  [Data Structures](http://swcarpentry.github.io/r-novice-gapminder/04-data-structures-part1/)
    5.  [Exploring Data Frames](http://swcarpentry.github.io/r-novice-gapminder/05-data-structures-part2/)
