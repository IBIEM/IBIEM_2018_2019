This website can be reached at: <https://bit.ly/2MyCyqM>

Bootcamp Morning Syllabus
-------------------------

| Date | Day |
|------|-----|
| 8/9  | 1   |
| 8/10 | 2   |
| 8/11 | 3   |
| 8/13 | 4   |
| 8/14 | 5   |

| Topic                                                         | Day   |
|---------------------------------------------------------------|-------|
| [Microbiome Analysis Overview](#microbiome-analysis-overview) | 1 & 2 |
| [RStudio](#rstudio) Computing Environment                     | 2 & 3 |
| [Reproducible Analysis](#reproducible-analysis)               | 3     |
| [Git](#git)                                                   | 3     |
| [Introduction to R](#introduction-to-r)                       | 3 & 5 |
| Marbles                                                       | 4     |

Microbiome Analysis Overview
----------------------------

Microbiome Analysis Overview [lecture slides](bootcamp/010_microbiome_analysis_overview.pdf)

RStudio
-------

1.  [UN Votes Visualization](bootcamp/020_unvotes.Rmd)
2.  Making a Notebook
3.  [Introduction to R and RStudio](http://swcarpentry.github.io//swc-releases/2016.06/r-novice-gapminder/01-rstudio-intro/)
4.  [Project Management With RStudio](http://swcarpentry.github.io//swc-releases/2016.06/r-novice-gapminder/02-project-intro/)

### Additional RStudio Resources

-   [Writing code in RStudio](https://www.rstudio.com/resources/webinars/rstudio-essentials-webinar-series-part-1/): A detailed introduction to RStudio

Reproducible Analysis
---------------------

-   [Reproducible Analysis Lecture Notes](bootcamp/030_reproducible_research.md)

Git
---

-   [Git Overview](bootcamp/040_git_overview.md)

Introduction to R
-----------------

1.  [Seeking Help](http://swcarpentry.github.io//swc-releases/2016.06/r-novice-gapminder/03-seeking-help/)
2.  [Data Structures](http://swcarpentry.github.io//swc-releases/2016.06/r-novice-gapminder/04-data-structures-part1/)
3.  [Exploring Data Frames](http://swcarpentry.github.io//swc-releases/2016.06/r-novice-gapminder/05-data-structures-part2/)
4.  [Subsetting Data](http://swcarpentry.github.io//swc-releases/2016.06/r-novice-gapminder/06-data-subsetting/)
5.  [Control Flow](http://swcarpentry.github.io//swc-releases/2016.06/r-novice-gapminder/07-control-flow/)
6.  [Creating Publication-Quality Graphics](http://swcarpentry.github.io//swc-releases/2016.06/r-novice-gapminder/08-plot-ggplot2/)
7.  [Vectorization](http://swcarpentry.github.io//swc-releases/2016.06/r-novice-gapminder/09-vectorization/)
8.  [Dataframe Manipulation with dplyr](http://swcarpentry.github.io//swc-releases/2016.06/r-novice-gapminder/13-dplyr/)
9.  [Dataframe Manipulation with tidyr](http://swcarpentry.github.io//swc-releases/2016.06/r-novice-gapminder/14-tidyr/)
10. [Producing Reports With knitr](http://swcarpentry.github.io//swc-releases/2016.06/r-novice-gapminder/15-knitr-markdown/)
11. [Writing Good Software](http://swcarpentry.github.io//swc-releases/2016.06/r-novice-gapminder/16-wrap-up/)

-   [R Lesson Reference](http://swcarpentry.github.io/r-novice-gapminder/reference)
